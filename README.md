# f5_assignment

This project is a test project, and it contains two files:

## test.py:

This is a PyTest python file, it includes two tests, each one of them checks if a certain string is included in the web-page using the requests library methods on the web-pages URL.

## .gitlab-ci.yml:

This file includes the test stage, it's triggered everytime we commit a new branch or a pull request. In my project, it has only one stage (test) as requested, and everytime this file is triggered it runs both of the tests in file 'test.py'. 
