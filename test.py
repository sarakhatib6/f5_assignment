import requests
import sys

url = "https://the-internet.herokuapp.com/context_menu"
response = requests.get(url)
response_text = response.text


def first_test():
    assert "Right-click in the box below to see one called 'the-internet'" in response_text


def second_test():
    assert "Alibaba" in response_text


test = sys.argv[1]


if test == "first_test":
    first_test()
elif test == "second_test":
    second_test()
